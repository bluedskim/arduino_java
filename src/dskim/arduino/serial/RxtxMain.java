package dskim.arduino.serial;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import org.apache.http.client.ClientProtocolException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 참고 : http://arduino-er.blogspot.kr/2013/06/create-java-project-using-librxtx-java.html
 * @author shed
 *
 */
public class RxtxMain implements SerialPortEventListener {
    public final Logger logger = LoggerFactory.getLogger(RxtxMain.class);
    
	SerialPort serialPort;
	HashMap<String, String> thingSpeakdataMap ;
	
	/** The port we're normally going to use. */
	private static final String PORT_NAMES[] = { 
			"/dev/ttyACM0", // for Ubuntu 
			"/dev/ttyACM1", // for Ubuntu 
			"/dev/ttyACM2", // for Ubuntu
			"/dev/ttyACM99", // for Ubuntu
			"/dev/tty.usbserial-A9007UX1", // Mac OS X
			"/dev/ttyUSB0", // Linux
			"/dev/ttyUSB1", // Linux
			"/dev/ttyUSB2", // Linux
			"COM3", // Windows
	};
	/**
	 * A BufferedReader which will be fed by a InputStreamReader converting the
	 * bytes into characters making the displayed results codepage independent
	 */
	private BufferedReader input;
	/** The output stream to the port */
	private OutputStream output;
	/** Milliseconds to block while waiting for port open */
	private static final int TIME_OUT = 2000;
	/** Default bits per second for COM port. */
	private static final int DATA_RATE = 9600;

	private SerialCommandHandler serialCommandHandler;

	public RxtxMain() {
		super();
		serialCommandHandler = new SerialCommandHandler();
	}

	public void initialize() {
		logger.debug("initializing");
		CommPortIdentifier portId = null;
		Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();

		// First, Find an instance of serial port as set in PORT_NAMES.
		while (portEnum.hasMoreElements()) {
			CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
			logger.debug("currPortId={}" + currPortId);
			for (String portName : PORT_NAMES) {
				if (currPortId.getName().equals(portName)) {
					portId = currPortId;
					logger.debug("portName={} portId={}", portName, portId);
					break;
				}
			}
		}
		if (portId == null) {
			logger.error("Could not find COM port.");
			return;
		}

		try {
			// open serial port, and use class name for the appName.
			serialPort = (SerialPort) portId.open(this.getClass().getName(),TIME_OUT);

			// set port parameters
			serialPort.setSerialPortParams(DATA_RATE, SerialPort.DATABITS_8,SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

			// open the streams
			input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
			output = serialPort.getOutputStream();

			// add event listeners
			serialPort.addEventListener(this);
			serialPort.notifyOnDataAvailable(true);
		} catch (Exception e) {
			logger.error("exception={}", e);
		}
		logger.debug("initialized");
	}

	/**
	 * This should be called when you stop using the port. This will prevent
	 * port locking on platforms like Linux.
	 */
	public synchronized void close() {
		if (serialPort != null) {
			serialPort.removeEventListener();
			serialPort.close();
		}
	}

	/**
	 * Handle an event on the serial port. Read the data and print it.
	 */
	public synchronized void serialEvent(SerialPortEvent oEvent) {
		//System.out.println("oEvent.getEventType()=" + oEvent.getEventType());
		if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			try {
				String inputLine = input.readLine();
				logger.debug(new Date() + "\t\t" + inputLine);
				//uploadSensorData(inputLine);
				serialCommandHandler.processCommand(inputLine);
			} catch (Exception e) {
				logger.error("exception={}", e);
			}
		}
		// Ignore all the other eventTypes, but you should consider the other
		// ones.
	}
	
	public static void main(String[] args) throws Exception {		
		RxtxMain main = new RxtxMain();
		main.initialize();
		Thread t = new Thread() {
			public void run() {
				// the following line will keep this app alive for 1000 seconds,
				// waiting for events to occur and responding to them (printing
				// incoming messages to console).
				try {
					Thread.sleep(1000000);
				} catch (InterruptedException ie) {
				}
			}
		};
		t.start();
	}
}