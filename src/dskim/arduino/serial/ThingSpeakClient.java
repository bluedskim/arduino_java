package dskim.arduino.serial;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.Future;

import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThingSpeakClient {
    private final Logger logger = LoggerFactory.getLogger(ThingSpeakClient.class);
	//String url = "https://api.thingspeak.com/update?key=4SKDAPHJ33N9GF1O";
    String url ;
	// ThingSpeak 가 의외로 응답속도가 느려서 Async 로 접속해야 함 
	CloseableHttpAsyncClient httpClient;
	HashMap<String, String> fieldNameMap;
	ShellCommander shellCommander;
	Properties arduinoJavaProp;
	//SayCurrHour 의 최대 볼륨
	int maxVolume = 80;
	//SayCurrHour 의 최소 볼륨
	int minVolume = 40;
	// 광센서의 최대값. 이걸 넘겨도 상관없음
	int maxPhotocellVal = 700;
	int httpTimeOut = 5;
	
	public ThingSpeakClient(HashMap<String, String> fieldNameMap) {
		super();
		logger.debug("initializing...");
		this.fieldNameMap = fieldNameMap;
		url = fieldNameMap.get("url");
		//client = HttpClientBuilder.create().build();
		RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(5 *1000).setConnectTimeout(5 *1000).build();
		httpClient = HttpAsyncClients.custom().setDefaultRequestConfig(requestConfig).build();
		httpClient.start();
		logger.debug("httpClient started... url={}", url);
		
		shellCommander = new ShellCommander();
		arduinoJavaProp = new Properties();
		try {
			arduinoJavaProp.load(getClass().getResourceAsStream("arduino_java.prop"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		
		
		httpTimeOut = Integer.parseInt((String) arduinoJavaProp.get("httpTimeOut"));
		logger.debug("httpTimeOut={}", httpTimeOut);
	}

	public void putData(HashMap<String, String> sensorData) throws Exception {
		logger.debug("puting data... sensorData={}", sensorData);
		String postingUrl = url;
		// URL 생성
	    for( String key : sensorData.keySet() ){
	    	postingUrl += "&" + fieldNameMap.get(key) + "=" + sensorData.get(key);
	
	    	// 광센서의 값(밝기에 따라 SayCurrHour의 볼륨 결정 ㅎㅎ
	  		if (key.contains("photocellVal")) {
	  			//String[] volSetCommand = { "echo", calcPhotocellToVolume("300"), ">", "/home/odroid/Util/shell/sayCurrHourAndMin/volume.txt"};
	  			//logger.debug("volSetCommand={}", volSetCommand[0] + " " + volSetCommand[1] + " " + volSetCommand[2] + " " + volSetCommand[3]);
	  			//shellCommander.shellCmd(volSetCommand);
	  			boolean saveSayCurrHourAndMinVolumeSuccess = saveSayCurrHourAndMinVolume(calcPhotocellToVolume(sensorData.get(key)), "/home/odroid/Util/shell/sayCurrHourAndMin/volume.txt");
	  			logger.debug("saveSayCurrHourAndMinVolumeSuccess={}", saveSayCurrHourAndMinVolumeSuccess);
	  		}		
	    }
	    logger.debug("postingUrl={}", postingUrl);
		HttpGet request = new HttpGet(postingUrl);
	    Future<HttpResponse> future = httpClient.execute(request, null);
	    HttpResponse response = future.get();	
		logger.debug("Response Code : ={}", response.getStatusLine().getStatusCode());
	}

	public void putData(String serialLine) throws Exception {
		HashMap<String, String> dataMap = new HashMap<String, String>();
		
		String sensorDataNameAndValues[] = serialLine.split("\t");
		for(String sensorDataNameAndValue : sensorDataNameAndValues) {
			String sensorDataNameAndValueArray[] = sensorDataNameAndValue.split(":");
			dataMap.put(sensorDataNameAndValueArray[0], sensorDataNameAndValueArray[1]); 
		}
		putData(dataMap);
	}

	boolean saveSayCurrHourAndMinVolume(String volumeVal, String filePath) {
		boolean isSuccess = false;
		try {
			File file = new File(filePath);
			FileWriter fw = new FileWriter(file);
			fw.write(volumeVal);
			fw.flush();
			fw.close();
			isSuccess = true;
		} catch (Exception e) {
			logger.error("error! {}", e);
		}
		return isSuccess;
	}
	
	String calcPhotocellToVolume(String photocellValstr) {		
		int volume = 0;
		int photocellVal = Integer.parseInt(photocellValstr);
		volume = maxVolume * photocellVal / maxPhotocellVal ;
		if(volume < minVolume) volume = minVolume;
		if(volume > maxVolume) volume = maxVolume;
		
		logger.debug("volume={}", volume);
		return String.valueOf(volume);
	}
}
