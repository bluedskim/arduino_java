package dskim.arduino.serial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShellCommander {
	private final Logger logger = LoggerFactory.getLogger(SerialCommandHandler.class);

	public boolean shellCmd(String command) throws Exception {
		String[] commandArr = { command };
		return shellCmd(commandArr);
	}

	public boolean shellCmd(String[] command) throws Exception {
		boolean isSuccess = false;
		Runtime runtime = Runtime.getRuntime();
		Process process = runtime.exec(command);
		return isSuccess;
	}
}