package dskim.arduino.serial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InternalCommander {
	private final Logger logger = LoggerFactory
			.getLogger(SerialCommandHandler.class);
	public static int metronomSpeed = 100;

	public boolean inVokeCommand(String shellBasePath, String irCommand) throws Exception {
		boolean isSuccess = false;
		logger.debug("irCommand={}", irCommand);
		if (irCommand.contains("metronom")) {
			logger.debug("메트로놈이다~");
			int speedIndicator = Integer.parseInt(irCommand.replace("metronom", ""));
			metronomSpeed = metronomSpeed + speedIndicator;
			if (metronomSpeed <= 0)
				metronomSpeed = 1;
			logger.debug("metronomSpeed={}", metronomSpeed);
			String[] commandArr = { shellBasePath + "metronom.sh", String.valueOf(metronomSpeed) };
			logger.debug("commandArr={}", commandArr);
			isSuccess = shellCmd(commandArr);
			logger.debug("isSuccess={}", isSuccess);
		} else if (irCommand.contains("volume")) {
			logger.debug("볼륨이다~");
			String volumeIndicator = irCommand.replace("volume", "");
			logger.debug("volumeIndicator={}", volumeIndicator);
			String[] commandArr = { "amixer", "-D", "pulse", "sset", "Master", volumeIndicator };
			logger.debug("commandArr={}", commandArr);
			isSuccess = shellCmd(commandArr);
			logger.debug("isSuccess={}", isSuccess);
		}
		return isSuccess;
	}

	private boolean shellCmd(String[] command) throws Exception {
		Runtime runtime = Runtime.getRuntime();
		Process process = runtime.exec(command);
		return true;
	}
}