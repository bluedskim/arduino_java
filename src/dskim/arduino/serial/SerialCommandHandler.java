package dskim.arduino.serial;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 시리얼 라인으로 들어오는 명령 String 을 받아서 해석하고 해당하는 명령을 처리 
 * @author shed
 *
humidity:nan	temperature:nan
photocellVal:341
flamelVal:338
Failed to read from DHT sensor!
humidity:nan	temperature:nan
photocellVal:341
flamelVal:338
irInput:FFB04F
irInput:FF9867
irInput:FF6897
irInput:FFFFFFFF
Failed to read from DHT sensor!
humidity:nan	temperature:nan
photocellVal:343
flamelVal:340
Failed to read from DHT sensor!
humidity:nan	temperature:nan
photocellVal:345
flamelVal:342
Failed to read from DHT sensor!
humidity:nan	temperature
 */

public class SerialCommandHandler {
	private final Logger logger = LoggerFactory.getLogger(SerialCommandHandler.class);

	ThingSpeakClient thingSpeak;
	ShellCommander shellCommander;
	InternalCommander internalCommander;
	Properties irCodeAliases;
	Properties irCodeToCommand;
	Properties thingSpeakdataNameMap;
	Properties arduinoJavaProp;

	public SerialCommandHandler() {
		super();

		irCodeAliases = new Properties();
		irCodeToCommand = new Properties();
		thingSpeakdataNameMap = new Properties();
		arduinoJavaProp = new Properties();
		try {
			irCodeAliases.load(getClass().getResourceAsStream("irCodeAliases.prop"));
			irCodeToCommand.load(getClass().getResourceAsStream("irCodeToCommand.prop"));
			thingSpeakdataNameMap.load(getClass().getResourceAsStream("thingSpeakdataNameMap.prop"));
			arduinoJavaProp.load(getClass().getResourceAsStream("arduino_java.prop"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		thingSpeak = new ThingSpeakClient(new HashMap(thingSpeakdataNameMap));
		shellCommander = new ShellCommander();
		internalCommander = new InternalCommander();
	}

	boolean processCommand(String serialLineIn) throws Exception {
		boolean isSuccess = false;
		logger.debug("serialLine={}", serialLineIn);
		if (serialLineIn.contains(":")) {
			logger.debug("nomal command");
			try {
				if (serialLineIn.contains("irInput")) {
					String irCode = serialLineIn.split(":")[1].toUpperCase();
					if (irCodeAliases.containsKey(irCode)) {
						String irCommand = irCodeToCommand.getProperty(irCodeAliases.getProperty(irCode));
						logger.debug("irCommand={}", irCommand);
						if (irCommand.contains(".sh")) {
							shellCommander.shellCmd(arduinoJavaProp.getProperty("shellBasePath") + irCommand);
						} else {
							internalCommander.inVokeCommand(arduinoJavaProp.getProperty("shellBasePath"), irCommand);
						}
					} else {
						logger.error("irCode={} 를 찾을 수 없음~! noise???", irCode);
					}
				} else {
					thingSpeak.putData(serialLineIn);
				}
			} catch (Exception e) {
				logger.error("Exception={}", e);
			}
		} else {
			logger.debug("not a command");
		}
		return isSuccess;
	}
}
